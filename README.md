# Backend

**introduction**
- https://app.pluralsight.com/library/courses/web-development-executive-briefing/table-of-contents
- https://app.pluralsight.com/library/courses/designing-restful-web-apis/table-of-contents


**c#**    
- https://app.pluralsight.com/library/courses/object-oriented-programming-fundamentals-csharp/table-of-contents


**asp net**
- https://app.pluralsight.com/library/courses/becoming-dotnet-developer/table-of-contents
- https://app.pluralsight.com/library/courses/aspnet-core-fundamentals/table-of-contents
- https://app.pluralsight.com/library/courses/asp-dot-net-core-3-restful-api-building/table-of-contents


**sql server**    
- https://app.pluralsight.com/library/courses/intro-sql-server/table-of-contents

____

# Frontend



**frontend**
- https://app.pluralsight.com/library/courses/front-end-web-development-get-started/table-of-contents
- https://app.pluralsight.com/library/courses/front-end-web-app-html5-javascript-css/table-of-contents
- https://app.pluralsight.com/library/courses/javascript-project-json/table-of-contents

**web design**
- https://learn.freecodecamp.org/responsive-web-design/basic-html-and-html5/ 
- https://learn.freecodecamp.org/responsive-web-design/basic-css/ 
- https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-principles/ 
- https://learn.freecodecamp.org/front-end-libraries/jquery/ 


**web development**
- https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-javascript/ 
- https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/
- https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/debugging/
- https://app.pluralsight.com/library/courses/fiddler-chrome-developer-tools-debugging-website/table-of-contents
- https://app.pluralsight.com/library/courses/tactics-tools-troubleshooting-front-end-web-development/table-of-contents


**introduction to angular**
- https://app.pluralsight.com/library/courses/ng-big-picture/table-of-contents
- https://app.pluralsight.com/library/courses/angular-2-getting-started-update/table-of-contents

- 
___
# tools & misc

- setup github account
- setup freecodecamp account
- install   
        - [vs code](https://code.visualstudio.com/download)   
        - [nodejs lts](https://nodejs.org/en/download/)   
        - [git](https://git-scm.com/downloads)    

- https://app.pluralsight.com/library/courses/git-fundamentals/table-of-contents
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://app.pluralsight.com/library/courses/using-gitflow/table-of-contents



